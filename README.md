# Better Internet - Icons #

[Here are the icons of "Better Internet" located.](https://bitbucket.org/nofudgecoders/better-internet-icons/src)

### Index ###
 - Clone repository
 - Fetch and pull changes
 - Commit and push changes
 - Contact

### Clone repository ###

```
cd <Local repositories>/
git clone https://bitbucket.org/nofudgecoders/better-internet-icons.git
```

### Fetch and pull changes ###
```
cd <Local repositories>/better-internet-icons/
git fetch origin
git pull origin master
```

### Commit and push changes ###

```
cd <Local repositories>/better-internet-icons
git add --all
git commit -m <Message in quotes>
git push --all origin
```

### Contact ###
[BigETI - Ethem Kurt](https://bitbucket.org/BigETI/)